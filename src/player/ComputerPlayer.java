package player;

import game.Direction;

import java.awt.Color;
import java.util.Random;

public class ComputerPlayer extends Player {
	int difficulty;

	public ComputerPlayer(String n, Direction d, int x, int y, Color c, int diff) {
		super(n, d, x, y, c);
		difficulty = diff;
	}

	public void chooseCards() {
		Random r = new Random();
		for (int i = damage; i < 5; i++) {
			if ((damage == 3) && (i == 3))
				return;
			if ((damage == 4) && (i == 2))
				return;
			register.add(hand.remove(r.nextInt(hand.size())));
		}
	}
}
