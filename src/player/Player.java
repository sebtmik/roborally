package player;

import game.Card;
import game.Direction;
import game.RoboRally;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class Player {
	List<Card> register;
	List<Card> hand;
	final String name;
	Color color;
	Direction dir;
	Direction startDir;
	int x, y;
	int startx, starty;
	int damage;
	int lifePoints;
	public boolean powerDown;

	public Player(String n, Direction d, int x, int y, Color c) {
		register = new ArrayList<Card>();
		name = n;
		color = c;
		dir = d;
		startDir = d;
		this.x = x;
		this.y = y;
		startx = x;
		starty = y;
		lifePoints = 3;
		damage = 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Player [name=" + name + ", dir=" + dir + ", x=" + x + ", y="
				+ y + ", damage=" + damage + ", lifePoints=" + lifePoints + "]";
	}

	public void dealCards(List<Card> cards) {
		hand = cards;
	}

	public Card getNextRegisterCard() {
		return register.remove(0);
	}

	/**
	 * @return the lifePoints
	 */
	public int getLifePoints() {
		return lifePoints;
	}

	/**
	 * @param lifePoints
	 *            the lifePoints to set
	 */
	public void setLifePoints(int lifePoints) {
		this.lifePoints = lifePoints;
	}

	public void changeDirection(Direction d) {
		dir = d;
	}

	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void destroyed() {
		lifePoints--;
		damage = 0;
		RoboRally.getBoard().getSquare(x, y).setPlayer(null);
		x = startx;
		y = starty;
		dir = startDir;
		System.out.println(name + " destroyed");
		RoboRally.getBoard().getSquare(startx, starty).setPlayer(this);
	}

	/*
	 * @return if damage was lethal
	 */
	public boolean takeDamage(int d) {
		if (d > 0) {
			damage += d;
			System.out
			.println(name + " took " + d + " damage, total:" + damage);
			if (damage >= 5) {
				destroyed();
				return true;
			}
		}
		return false;
	}

	public int getDamage() {
		return damage;
	}

	public Direction getDirection() {
		return dir;
	}

	public void setDirection(Direction d) {
		dir = d;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public boolean hasRegCard() {
		return register.size() > 0;
	}

	public int getNextCardPriority() {
		return register.get(0).getPriority();
	}

	public String getName() {
		return name;
	}

	public Color getColor() {
		return color;
	}
}
