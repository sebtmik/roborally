package ui;

import game.Board;
import game.Direction;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.RenderingHints;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.MatteBorder;

public class Gui extends JFrame {
	static Board board;
	List<Square> squares;

	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			try {
				UIManager.setLookAndFeel(UIManager
						.getSystemLookAndFeelClassName());
				Gui window = new Gui(new Board(10, 10, 10, 10));
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	public Gui(Board b) {
		board = b;
		squares = new ArrayList<Gui.Square>();
		initialize();
		// this.pack();
		this.setVisible(true);
	}

	private void initialize() {
		this.setMinimumSize(new Dimension(800, 640));
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(
				new GridLayout(board.getHeight(), board.getWidth(), 5, 5));
		int i = 0;
		int j = 0;
		for (List<game.Square> sqr : board.getSquares()) {
			j = 0;
			for (game.Square sq : sqr) {
				Square s = new Square(sq, j++, i);
				squares.add(s);
				getContentPane().add(s);
			}
			i++;
		}
	}

	public static class Square extends JPanel {
		game.Square sq;
		int x, y;

		public Square(game.Square s, int i, int j) {
			super();
			sq = s;
			x = i;
			y = j;
			this.setBackground(Color.white);
			Border b = null;
			if (sq.hasWall(Direction.NORTH)) {
				b = new MatteBorder(5, 0, 0, 0, Color.BLACK);
			}
			if (sq.hasWall(Direction.SOUTH)) {
				b = new MatteBorder(0, 0, 5, 0, Color.BLACK);
			}
			if (sq.hasWall(Direction.EAST)) {
				b = new MatteBorder(0, 5, 0, 0, Color.BLACK);
			}
			if (sq.hasWall(Direction.WEST)) {
				b = new MatteBorder(0, 0, 0, 5, Color.BLACK);
			}
			if (b != null) {
				this.setBorder(b);
			}
		}

		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D g2d = (Graphics2D) g;
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON);
			g2d.drawString("Belt: " + sq.getBelt(), 5, 15);
			g2d.drawString(x + "," + y, 50, 15);
			g2d.drawString("Laser: " + sq.getLaser(), 5, 25);
			if (sq.hasPlayer()) {
				drawCenteredCircle(g2d, 15, 35, 20, sq.getPlayer().getColor());
				g2d.drawString(sq.getPlayer().getDirection().toString(), 5, 55);
			}
			if (sq.isHole()) {
				drawCenteredCircle(g2d, 50, 40, 25, Color.BLACK);
			}

		}

		public void drawCenteredCircle(Graphics2D g, int x, int y, int r,
				Color c) {
			g.setColor(c);
			x = x - (r / 2);
			y = y - (r / 2);
			g.fillOval(x, y, r, r);
		}
	}
}
