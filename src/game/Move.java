package game;

import player.Player;

public class Move {

	public Move() {

	}

	public void turn(Player player, Type t) {
		Direction dir = player.getDirection();
		if (t == Type.RIGHT) {
			if (dir == Direction.NORTH) {
				player.setDirection(Direction.EAST);
			}
			if (dir == Direction.SOUTH) {
				player.setDirection(Direction.WEST);
			}
			if (dir == Direction.EAST) {
				player.setDirection(Direction.SOUTH);
			}
			if (dir == Direction.WEST) {
				player.setDirection(Direction.NORTH);
			}
		}
		if (t == Type.LEFT) {
			if (dir == Direction.NORTH) {
				player.setDirection(Direction.WEST);
			}
			if (dir == Direction.SOUTH) {
				player.setDirection(Direction.EAST);
			}
			if (dir == Direction.EAST) {
				player.setDirection(Direction.NORTH);
			}
			if (dir == Direction.WEST) {
				player.setDirection(Direction.SOUTH);
			}
		}
		if (t == Type.UTURN) {
			if (dir == Direction.NORTH) {
				player.setDirection(Direction.SOUTH);
			}
			if (dir == Direction.SOUTH) {
				player.setDirection(Direction.NORTH);
			}
			if (dir == Direction.EAST) {
				player.setDirection(Direction.WEST);
			}
			if (dir == Direction.WEST) {
				player.setDirection(Direction.EAST);
			}
		}
		postMove(player);
	}

	/**
	 * @return if the robot was moved (or destroyed)
	 */
	public boolean move(Player player, int num, Direction dir) {
		if (dir != null) {
			System.out.println(player.getName() + " getting pushed " + dir);
		}

		boolean ret = false;
		int x = player.getX();
		int y = player.getY();
		System.out.println("\nX" + x + ",Y" + y);
		Square sq = RoboRally.getBoard().getSquare(x, y);
		Square next = null;
		if (num == -1) {
			if (dir == null) {
				dir = player.getDirection();
			}
			if (dir == Direction.NORTH) {
				if (sq.hasWall(dir)) {
					ret = false;
				} else {
					if (y >= (RoboRally.getBoard().getHeight() - 1)) {
						player.destroyed();
						System.out.println("Outside " + dir);
						return true;
					} else {
						next = RoboRally.getBoard().getSquare(x, y + 1);
						if (!next.hasWall(Direction.NORTH)) {
							y = y + 1;
						}
					}
				}
			}
			if (dir == Direction.SOUTH) {
				if (sq.hasWall(dir)) {
					ret = false;
				} else {
					if (y <= 0) {
						player.destroyed();
						System.out.println("Outside " + dir);
						return true;
					} else {
						next = RoboRally.getBoard().getSquare(x, y - 1);
						if (!next.hasWall(Direction.SOUTH)) {
							y = y - 1;
						}
					}
				}
			}
			if (dir == Direction.EAST) {
				if (sq.hasWall(dir)) {
					ret = false;
				} else {
					if (x <= 0) {
						player.destroyed();
						System.out.println("Outside " + dir);
						return true;
					} else {
						next = RoboRally.getBoard().getSquare(x - 1, y);
						if (!next.hasWall(Direction.EAST)) {
							x = x - 1;
						}
					}
				}
			}
			if (dir == Direction.WEST) {
				if (sq.hasWall(dir)) {
					ret = false;
				} else {
					if (x >= (RoboRally.getBoard().getWidth() - 1)) {
						System.out.println("Outside " + dir);
						player.destroyed();
						return true;
					} else {
						next = RoboRally.getBoard().getSquare(x + 1, y);
						if (!next.hasWall(Direction.WEST)) {
							x = x + 1;
						}
					}
				}
			}
			if ((next != null) && next.hasPlayer()) {
				System.out.println(player.getName() + " pushing " + dir);
				if (!move(next.getPlayer(), 1, dir)) {
					ret = false;
				} else {
					sq.setPlayer(null);
					player.setPosition(x, y);
					RoboRally.getBoard().getSquare(x, y).setPlayer(player);
					ret = true;
				}
			} else {
				sq.setPlayer(null);
				player.setPosition(x, y);
				RoboRally.getBoard().getSquare(x, y).setPlayer(player);
				ret = true;
			}
			if (postMove(player))
				return true;
		} else {
			for (int i = 0; i < num; i++) {
				sq = RoboRally.getBoard().getSquare(x, y);
				next = null;
				if (dir == null) {
					dir = player.getDirection();
				}
				if (dir == Direction.NORTH) {
					if (sq.hasWall(dir)) {
						ret = false;
					} else {
						if (y <= 0) {
							player.destroyed();
							System.out.println("Outside " + dir);
							return true;
						} else {
							next = RoboRally.getBoard().getSquare(x, y - 1);
							if (!next.hasWall(Direction.SOUTH)) {
								y = y - 1;
							}
						}
					}
				}
				if (dir == Direction.SOUTH) {
					if (sq.hasWall(dir)) {
						ret = false;
					} else {
						if (y >= (RoboRally.getBoard().getHeight() - 1)) {
							player.destroyed();
							System.out.println("Outside " + dir);
							return true;
						} else {
							next = RoboRally.getBoard().getSquare(x, y + 1);
							if (!next.hasWall(Direction.NORTH)) {
								y = y + 1;
							}
						}
					}
				}
				if (dir == Direction.EAST) {
					if (sq.hasWall(dir)) {
						ret = false;
					} else {
						if (x >= (RoboRally.getBoard().getWidth() - 1)) {
							player.destroyed();
							System.out.println("Outside " + dir);
							return true;
						} else {
							next = RoboRally.getBoard().getSquare(x + 1, y);
							if (!next.hasWall(Direction.WEST)) {
								x = x + 1;
							}
						}
					}
				}
				if (dir == Direction.WEST) {
					if (sq.hasWall(dir)) {
						ret = false;
					} else {
						if (x <= 0) {
							player.destroyed();
							System.out.println("Outside " + dir);
							return true;
						} else {
							next = RoboRally.getBoard().getSquare(x - 1, y);
							if (!next.hasWall(Direction.EAST)) {
								x = x - 1;
							}
						}
					}
				}
				if ((next != null) && next.hasPlayer()) {
					System.out.println(player.getName() + " pushing " + dir);
					if (!move(next.getPlayer(), 1, dir)) {
						ret = false;
						break;
					} else {
						sq.setPlayer(null);
						player.setPosition(x, y);
						RoboRally.getBoard().getSquare(x, y).setPlayer(player);
						ret = true;
					}
				} else {
					sq.setPlayer(null);
					player.setPosition(x, y);
					RoboRally.getBoard().getSquare(x, y).setPlayer(player);
					ret = true;
				}
				if (postMove(player))
					return true;
			}
		}
		return ret;
	}

	public void run(Player player, Card card) {
		System.out.println(player.getName() + " " + card);
		int x = player.getX();
		int y = player.getY();
		Square s = RoboRally.getBoard().getSquare(x, y);
		if (card.getType() == Type.MOVE1) {
			move(player, 1, null);
			if (player.getLifePoints() <= 0)
				return;
		}
		if (card.getType() == Type.MOVE2) {
			move(player, 2, null);
			if (player.getLifePoints() <= 0)
				return;
		}
		if (card.getType() == Type.MOVE3) {
			move(player, 3, null);
			if (player.getLifePoints() <= 0)
				return;
		}
		if (card.getType() == Type.BACKUP) {
			move(player, -1, null);
			if (player.getLifePoints() <= 0)
				return;
		}
		if (card.getType() == Type.LEFT) {
			turn(player, Type.LEFT);
			if (player.getLifePoints() <= 0)
				return;
		}
		if (card.getType() == Type.RIGHT) {
			turn(player, Type.RIGHT);
			if (player.getLifePoints() <= 0)
				return;
		}
		if (card.getType() == Type.UTURN) {
			turn(player, Type.UTURN);
			if (player.getLifePoints() <= 0)
				return;
		}
		belt(player);
	}

	public void belt(Player player) {
		Square s = RoboRally.getBoard().getSquare(player.getX(), player.getY());
		if (s.getBelt() > 0) {
			System.out.println("Belt:" + s.getBelt());
			move(player, s.getBelt(), null);
		}
	}

	/*
	 * @return whether robot still in play
	 */
	public static boolean postMove(Player p) {
		int x = p.getX();
		int y = p.getY();
		Square s = RoboRally.getBoard().getSquare(x, y);
		if (s.isHole()) {
			System.out.println("Hole");
			p.destroyed();
			return false;
		}
		if (p.takeDamage(s.getLaser()))
			return false;
		return true;
	}
}
