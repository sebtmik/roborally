package game;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Board {
	List<List<Square>> squares;
	int width, height;

	public Board(int w, int h, int gX, int gY) {
		// initiation
		squares = new ArrayList<List<Square>>();
		height = h;
		width = w;
		Random r = new Random();
		for (int i = 0; i < h; i++) {
			List<Square> s = new ArrayList<Square>();
			for (int j = 0; j < w; j++) {
				int t = r.nextInt(20);
				List<Direction> walls = new ArrayList<Direction>();
				if ((t == 10) || (t == 11)) {
					walls.add(Direction.getRandomDirection());
				}
				if ((i == gY) && (j == gX)) {
					s.add(new Square(0, 0, false, walls, true));
				} else if ((t == 1) || (t == 2) || (t == 3)) {
					s.add(new Square(r.nextInt(3) + 1, 0, false, walls, false));
				} else if ((t == 4) || (t == 5) || (t == 6)) {
					s.add(new Square(0, r.nextInt(3) + 1, true, walls, false));
				} else {
					s.add(new Square(0, 0, false, walls, false));
				}

			}
			squares.add(s);
		}

	}

	public Square getSquare(int x, int y) {
		return squares.get(y).get(x);
	}

	/**
	 * @return the squares
	 */
	public List<List<Square>> getSquares() {
		return squares;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
}
