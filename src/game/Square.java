package game;

import java.util.List;

import player.Player;

public class Square {
	List<Direction> walls;
	Player player;
	final private int laser;
	final private int belt;
	final private boolean hole;
	final private boolean goal;

	public Square(int l, int b, boolean h, List<Direction> w, boolean g) {
		player = null;
		laser = l;
		belt = b;
		hole = h;
		walls = w;
		goal = g;
	}

	public boolean hasWall(Direction dir) {
		for (Direction d : walls) {
			if (dir == d)
				return true;
		}
		return false;
	}

	public void setPlayer(Player p) {
		player = p;
	}

	public Player getPlayer() {
		return player;
	}

	public boolean hasPlayer() {
		return player != null;
	}

	/**
	 * @return the laser
	 */
	public int getLaser() {
		return laser;
	}

	/**
	 * @return the belt
	 */
	public int getBelt() {
		return belt;
	}

	/**
	 * @return the hole
	 */
	public boolean isHole() {
		return hole;
	}
}
