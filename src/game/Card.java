package game;

public class Card {
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Card [type=" + type + "]";
	}

	int priority;
	Type type;

	public Card(int pri, Type t) {
		priority = pri;
		type = t;
	}

	public int getPriority() {
		return priority;
	}

	public Type getType() {
		return type;
	}
}
