package game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import player.Player;

public class Deck {
	List<Card> deck;
	int num;

	public Deck() {
		deck = new ArrayList<Card>();
		num = 0;
		int pri = 1;

		for (Type type : Type.values()) {
			for (int i = 0; i < getCardAmount(type); i++) {
				deck.add(new Card(pri++ * 10, type));
			}
		}
		shuffle();
	}

	public int getCardAmount(Type type) {
		switch (type) {
		case MOVE1:
			return 18;
		case MOVE2:
			return 12;
		case MOVE3:
			return 6;
		case BACKUP:
			return 6;
		case LEFT:
			return 18;
		case RIGHT:
			return 18;
		case UTURN:
			return 6;
		default:
			return 0;
		}
	}

	public void shuffle() {
		Collections.shuffle(deck);
	}

	public Card getCard() {
		return deck.get(num++);
	}

	public void deal() {
		// for (Player player : getPlayers()) {
		// List<Card> cards = new ArrayList<Card>();
		// for (int i = player.getDamage(); i < 9; i++) {
		// cards.add(getDeck().getCard());
		// }
		// player.dealCards(cards);
		// }
		for (Player player : RoboRally.getPlayers()) {
			List<Card> cards = new ArrayList<Card>();
			for (int i = 0; i < 9; i++) {
				cards.add(RoboRally.getDeck().getCard());
			}
			player.dealCards(cards);
		}
	}
}
