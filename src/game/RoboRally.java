package game;

import java.awt.Color;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.UIManager;

import player.ComputerPlayer;
import player.Player;
import ui.Gui;

public class RoboRally {
	static Board board;
	static List<Player> players;
	static List<Player> lost;
	static List<Player> nextTurn;
	static Deck deck;
	static boolean waiting;
	static boolean playing;
	static int goalX, goalY;
	public static Player winner;
	static Gui window;
	static Move move;

	public static void main(String[] args) {
		waiting = true;
		playing = false;
		winner = null;
		lost = new ArrayList<Player>();
		nextTurn = new ArrayList<Player>();
		move = new Move();
		List<Player> p = new ArrayList<Player>();
		p.add(new ComputerPlayer("1", Direction.EAST, 5, 5, Color.RED, 0));
		p.add(new ComputerPlayer("2", Direction.EAST, 5, 6, Color.BLUE, 0));
		goalX = 8;
		goalY = 9;
		Board board = new Board(10, 10, goalX, goalX);
		setupGame(p, board);
		EventQueue.invokeLater(() -> {
			try {
				UIManager.setLookAndFeel(UIManager
						.getSystemLookAndFeelClassName());
				window = new Gui(board);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		while (waiting) {

		}
		while (playing) {
			deck = new Deck();
			getDeck().deal();
			for (int i = 0; i < players.size(); i++) {
				ComputerPlayer cp = (ComputerPlayer) players.get(i);
				cp.chooseCards();
			}
			while (playing && playerHasCards()) {
				nextTurn();
				for (Player t : nextTurn) {
					// new Scanner(System.in).nextLine();
					move.run(t, t.getNextRegisterCard());
					shootout();
					try {
						window.getContentPane().revalidate();
						window.getContentPane().repaint();
					} catch (NullPointerException e) {
						e.printStackTrace();
					}
					if (checkWon()) {
						System.out.println("Winner: " + winner);
						for (Player l : lost) {
							System.out.println("Loser: " + l);
						}
						System.exit(0);
					}
				}
			}

		}
	}

	private static boolean playerHasCards() {
		for (Player p : players)
			if (p.hasRegCard())
				return true;
		return false;
	}

	public static void nextTurn() {
		nextTurn = new ArrayList<Player>();
		for (Player p : players) {
			if (p.hasRegCard()) {
				nextTurn.add(p);
			}
		}
		Collections.sort(nextTurn, new PriorityComparator());
	}

	private static boolean checkWon() {
		if (winner != null)
			return true;
		for (Player player : players) {
			if ((player.getX() == goalX) && (player.getY() == goalY)) {
				winner = player;
				playing = false;
				return true;
			}
			if (player.getLifePoints() <= 0) {
				lost.add(player);
				players.remove(player);
			}
			if (players.size() == 1) {
				winner = players.get(0);
				playing = false;
				return true;
			}
		}
		return false;
	}

	public static List<Player> getPlayers() {
		return players;
	}

	public static Deck getDeck() {
		return deck;
	}

	public ComputerPlayer createComputerPlayer(int diff) {
		// TODO dynamic shit
		return new ComputerPlayer("", Direction.EAST, 10, 10, Color.RED, diff);
	}

	public static void setupGame(List<Player> p, Board b) {
		players = p;
		board = b;
		for (Player player : players) {
			int x = player.getX();
			int y = player.getY();
			board.getSquare(x, y).setPlayer(player);
		}
		deck = new Deck();
		waiting = false;
		playing = true;
	}

	public static void shootout() {
		for (Player player : getPlayers()) {
			Direction dir = player.getDirection();
			int x = player.getX();
			int y = player.getY();
			if (dir == Direction.NORTH) {
				for (int i = y; i > 0; i--) {
					Square t = getBoard().getSquare(x, i);
					if (!t.hasWall(Direction.NORTH)
							|| !t.hasWall(Direction.SOUTH))
						return;
					if (t.hasPlayer()) {
						t.getPlayer().takeDamage(1);
						return;
					}
				}
			}
			if (dir == Direction.SOUTH) {
				for (int i = y; i < getBoard().getHeight(); i++) {
					Square t = getBoard().getSquare(x, i);
					if (!t.hasWall(Direction.NORTH)
							|| !t.hasWall(Direction.SOUTH))
						return;
					if (t.hasPlayer()) {
						t.getPlayer().takeDamage(1);
						return;
					}
				}
			}
			if (dir == Direction.EAST) {
				for (int i = x; i < getBoard().getWidth(); i++) {
					Square t = getBoard().getSquare(i, y);
					if (!t.hasWall(Direction.WEST)
							|| !t.hasWall(Direction.EAST))
						return;
					if (t.hasPlayer()) {
						t.getPlayer().takeDamage(1);
						return;
					}
				}
			}
			if (dir == Direction.WEST) {
				for (int i = x; i > 0; i--) {
					Square t = getBoard().getSquare(i, y);
					if (!t.hasWall(Direction.WEST)
							|| !t.hasWall(Direction.EAST))
						return;
					if (t.hasPlayer()) {
						t.getPlayer().takeDamage(1);
						return;
					}
				}
			}
		}
	}

	/**
	 * @return the board
	 */
	public static Board getBoard() {
		return board;
	}
}

class PriorityComparator implements Comparator<Player> {
	@Override
	public int compare(Player a, Player b) {
		return b.getNextCardPriority() < a.getNextCardPriority() ? -1 : b
				.getNextCardPriority() == a.getNextCardPriority() ? 0 : 1;
	}
}
