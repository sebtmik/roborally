- Nesten ingenting er testet, så forvent bugs.
- Gui er bare en placeholder for debugging, så se bort ifra det.
- Main metoden simulerer et spill mellom to 'ComputerPlayers', så det er
også bare en placeholder.
- Det er ikke implementert noen menneskelige spillere, men det skal ikke
mye til, for det meste vise og velge kort, men dette er knyttet til GUI.
- Oppsett av nytt spill er så langt hardkodet, men det er også knyttet
til GUI.
- Koden er ikke kommentert eller strukturert.